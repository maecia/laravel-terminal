<?php

$router = $this->app->router;

$router->group(
    [
        'namespace' => 'Recca0120\Terminal\Http\Controllers',
        'prefix'    => 'terminal'
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'terminal.index',
            'uses' => 'TerminalController@index'
        ]);
        $router->post('endpoint', [
            'as'   => 'terminal.endpoint',
            'uses' => 'TerminalController@endpoint',
        ]);
    }
);
// Route::get('/media/{file}', [
//     'as' => 'media',
//     'uses' => 'TerminalController@media',
// ])->where(['file' => '.+']);
