<?php

namespace Recca0120\Terminal\Console\Commands;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\StringInput;
use Illuminate\Contracts\Console\Kernel as ArtisanContract;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Drush extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'drush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'drush command';

    /**
     * Handle the command.
     *
     * @throws \InvalidArgumentException
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $command = ['./vendor/bin/drush'];
        $action  = $this->argument('action');

        if ($action) {
            $command[] = $action;
        }

        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->line($process->getOutput());
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['action', null, InputOption::VALUE_REQUIRED],
        ];
    }
}
