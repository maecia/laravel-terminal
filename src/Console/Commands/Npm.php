<?php

namespace Recca0120\Terminal\Console\Commands;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\StringInput;
use Illuminate\Contracts\Console\Kernel as ArtisanContract;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Npm extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'npm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'npm command';

    /**
     * Handle the command.
     *
     * @throws \InvalidArgumentException
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $command = ['npm'];
        $action  = $this->argument('action');
        $option  = $this->argument('option');
        $prefix  = $this->option('prefix');

        if (!in_array($action, ['run', 'update'])) {
            throw \InvalidArgumentException;
        }

        if ($prefix) {
            $path      = $prefix;
            $command[] = '--prefix';

            if (!realpath($prefix)) {
                $path = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $prefix;
            }

            $command[] = $path;
        }

        if ($action) {
            $command[] = $action;
        }

        if ($option) {
            $command[] = $option;
        }

        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->line($process->getOutput());
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['action', null, InputOption::VALUE_REQUIRED],
            ['option', null, InputOption::VALUE_OPTIONAL],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['prefix', 'p', InputOption::VALUE_OPTIONAL, 'Prefix path'],
        ];
    }
}
