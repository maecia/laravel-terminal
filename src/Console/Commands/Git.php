<?php

namespace Recca0120\Terminal\Console\Commands;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\StringInput;
use Illuminate\Contracts\Console\Kernel as ArtisanContract;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Git extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'git';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'git command';

    /**
     * Handle the command.
     *
     * @throws \InvalidArgumentException
     */
    public function handle()
    {
        $action = $this->argument('action');

        if (!in_array($action, ['pull'])) {
            throw \InvalidArgumentException;
        }

        $process = new Process(['git', $action]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->line($process->getOutput());
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['action', null, InputOption::VALUE_REQUIRED],
        ];
    }
}
